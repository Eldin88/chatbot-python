# !/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.


from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, ParseMode)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
import sys, logging, requests, time, math

bot_token = sys.argv[1]  # Le premier paramètre (0) étant le nom du script (fichier)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
name = 'main'
logger = logging.getLogger(name)

START, SORTIR_CHOIX, RESTAURANTS_CHOIX, RESTO_RESULTATS, CARTES, RETOUR, TRANSPORT = range(7)


def start(bot, update):
    reply_keyboard = [['Restaurants', 'Sorties']]

    update.message.reply_text(
        'Salut! Je suis RestoBot alias le meilleur du milleu\n\n'
        'Que veux-tu faire ce soir ? Plutôt sortir ou aller au restaurant ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return START


def sortir_choix(bot, update):
    reply_keyboard = [['Musées', 'Bars', 'Clubs', 'Restaurants', 'Retour']]

    user = update.message.from_user
    logger.info("Volontée de %s: %s", user.first_name, update.message.text)
    update.message.reply_text(
        'Cool! Tu préfère sortir alors on va te trouver une activité,\n\n '
        'Voici les différentes activités possibles ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return SORTIR_CHOIX


def musees(bot, update):
    reply_keyboard = [['Retour']]

    update.message.reply_text(
        'Parfait! Il y a quelqun de cultivé en face de moi.\n'
        'Voici les 3 meilleurs musées de la ville:\n\nMusée d\'ethnographie de Genève\nFondation Baur Musée des arts d\'Extrême-Orient\nMuséum d\'histoire naturelle de Genève',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RETOUR


def bars(bot, update):
    reply_keyboard = [['Retour']]

    update.message.reply_text(
        'Un verre avec des amis ?.\n\n'
        'Ces bars sont sympas :\n\nSheera Mixologie\n\t\tRue Philippe-Plantamour 19, 1201 Genève\n\nLe Kraken\n\t\tRue de lEcole-de-Médecine 8, 1205 Genève\n\nFloortwo\n\t\tQuai du Mont-Blanc 19, 1201 Genève',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RETOUR


def clubs(bot, update):
    reply_keyboard = [['Retour']]

    update.message.reply_text(
        'Alors comme ça on veut danser ?.\n'
        'Les clubs suivants sont réputés pour être les meilleurs: \n\n Le Point Bar\nLe Moulin Rouge\nLe Petit Palace',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RETOUR


def restaurant_choix(bot, update):
    reply_keyboard = [['Italien', 'Asiatique', 'Oriental', 'Espagnole', 'Suisse', 'Retour']]

    user = update.message.from_user
    logger.info("%s a choisis %s", user.first_name, update.message.text)
    update.message.reply_text(
        'La chose la plus importante est de manger et surtout dans un bon restaurant, '
        'Voici ce que je te propose: ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RESTAURANTS_CHOIX


def Asiatique(bot, update):
    reply_keyboard = [['Chez Sunny', 'Wang', 'Naya', 'Liu Garden', 'Retour']]

    update.message.reply_text(
        'Voici les restaurants ouverts\n\n'
        'Lequel souhaites-tu découvrir ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RESTO_RESULTATS

def Italien(bot, update):
    reply_keyboard = [['La Petite Vendée', 'Luigia', 'La Gionconda', 'Molino', 'Retour']]

    update.message.reply_text(
        'Voici les restaurants ouverts\n\n'
        'Lequel souhaites-tu découvrir ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RESTO_RESULTATS

def Oriental(bot, update):
    reply_keyboard = [['Sofra', 'Al-Amir', 'Arabesque', 'Royal Oriental', 'Retour']]

    update.message.reply_text(
        'Voici les restaurants ouverts\n\n'
        'Lequel souhaites-tu découvrir ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RESTO_RESULTATS

def Espagnole(bot, update):
    reply_keyboard = [['El Faro', 'El Ruedo', 'Port Saladin', 'ArteAndaluz', 'Retour']]

    update.message.reply_text(
        'Voici les restaurants ouverts\n\n'
        'Lequel souhaites-tu découvrir ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RESTO_RESULTATS

def Suisse(bot, update):
    reply_keyboard = [['Café du Soleil', 'Bayview', 'Coulouvrenière', 'Les Armures', 'Retour']]

    update.message.reply_text(
        'Voici les restaurants ouverts\n\n'
        'Lequel souhaites-tu découvrir ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RESTO_RESULTATS


def Asiatique1(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.200472, 6.139456)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Asiatique2(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.203517, 6.157177)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Asiatique3(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.201546, 6.132115)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Asiatique4(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.199247, 6.132486)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Italien1(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.190034, 6.121417)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Italien2(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.224488, 6.126074)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Italien3(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.225924, 6.105717)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Italien4(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.203543, 6.148181)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Oriental1(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.199800, 6.133396)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Oriental2(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.210358, 6.146156)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Oriental3(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.214172, 6.151597)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Oriental4(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.224159, 6.108980)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Espagnole1(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.210915, 6.144987)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Espagnole2(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.211123, 6.145545)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Espagnole3(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.255313, 6.154995)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Espagnole4(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.212392, 6.113264)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Suisse1(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.223572, 6.127655)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Suisse2(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.214153, 6.151745)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Suisse3(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.204680, 6.138305)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def Suisse4(bot, update):
    reply_keyboard = [['Autres restaurants', 'GO']]
    update.message.reply_location(46.201563, 6.147170)
    update.message.reply_text(
        'Exellent choix !\n\n'
        'Clique sur GO pour avoir des informations sur les transports près de chez toi :)',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def appeler_opendata(path):
    url = "http://transport.opendata.ch/v1/" + path
    print(url)
    reponse = requests.get(url)
    return reponse.json()

def calcul_temps_depart(timestamp):
    seconds = timestamp-time.time()
    minutes = math.floor(seconds/60)
    if minutes < 1:
        return "FAUT COURIR!"
    if minutes > 60:
        return "> {} h.".format(math.floor(minutes/60))
    return "dans {} min.".format(minutes)


def afficher_arrets(update, arrets):
    texte_de_reponse = "Voici les arrets:\n"
    for station in arrets["stations"]:
        if station["id"] is not None:
            texte_de_reponse += "\n Arret: " + station["name"]

    update.message.reply_text(texte_de_reponse)

def afficher_departs(update, departs):
    texte_de_reponse = "Voici les prochains departs:\n\n"
    for depart in departs['stationboard']:
        texte_de_reponse += "{} {} dest. {} - {}\n".format(
            depart['category'],
            depart['number'],
            depart['to'],
            calcul_temps_depart(depart['stop']['departureTimestamp'])
        )
    texte_de_reponse += "\nAfficher a nouveau: /a" + departs['station']['id']

    coordinate = departs['station']['coordinate']
    update.message.reply_location(coordinate['x'], coordinate['y'])
    update.message.reply_text(texte_de_reponse)



def bienvenue(bot, update):
    update.message.reply_text("Peux-tu envoyer ta localisation stp ?",
                              reply_markup=ReplyKeyboardRemove())
    return TRANSPORT


def lieu_a_chercher(bot, update):
    resultats_opendata = appeler_opendata("locations?query=" + update.message.text)
    afficher_arrets(update, resultats_opendata)
    return TRANSPORT


def coordonnees_a_traiter(bot, update):
    location = update.message.location
    resultats_opendata = appeler_opendata("locations?x={}&y={}".format(location.latitude, location.longitude))
    afficher_arrets(update, resultats_opendata)
    return TRANSPORT


def details_arret(bot, update):
    update.message.reply_text("Details concernant un arrêt")
    return TRANSPORT





def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Ciao! À très bientôt :)',
    reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():

    updater = Updater(bot_token)

    dp = updater.dispatcher


    conv_handler_discussion = ConversationHandler(
        entry_points=[CommandHandler('start', start)],       
        states={
            START:
                [
                    RegexHandler('^(Sorties)$', sortir_choix),
                    RegexHandler('^(Restaurants)$', restaurant_choix)
                ],

            SORTIR_CHOIX:
                [
                    RegexHandler('^(Musées)$', musees),
                    RegexHandler('^(Bars)$', bars),
                    RegexHandler('^(Clubs)$', clubs),
                    RegexHandler('^(Restaurants)$', restaurant_choix),
                    RegexHandler('^(Retour)$', start)
                ],
            RETOUR:
                [
                    RegexHandler('^(Retour)$', sortir_choix)
                ],

            RESTAURANTS_CHOIX:
                [
                    RegexHandler('^(Italien)$', Italien),
                    RegexHandler('^(Asiatique)$', Asiatique),
                    RegexHandler('^(Oriental)$', Oriental),
                    RegexHandler('^(Espagnole)$', Espagnole),
                    RegexHandler('^(Suisse)$', Suisse),
                    RegexHandler('^(Retour)$', start)
                ],

            RESTO_RESULTATS:
                [
                    RegexHandler('^(La Petite Vendée)$', Italien1),
                    RegexHandler('^(Luigia)$', Italien2),
                    RegexHandler('^(La Gionconda)$', Italien3),
                    RegexHandler('^(Molino)$', Italien3),
           
                    RegexHandler('^(Chez Sunny)$', Asiatique1),
                    RegexHandler('^(Wang)$', Asiatique2),
                    RegexHandler('^(Naya)$', Asiatique3),
                    RegexHandler('^(Liu Garden)$', Asiatique4),
               
                    RegexHandler('^(Sofra)$', Oriental1),
                    RegexHandler('^(Al-Amir)$', Oriental2),
                    RegexHandler('^(Arabesque)$', Oriental3),
                    RegexHandler('^(Royal Oriental)$', Oriental4),
                
                    RegexHandler('^(El Faro)$', Espagnole1),
                    RegexHandler('^(El Ruedo)$', Espagnole2),
                    RegexHandler('^(Port Saladin)$', Espagnole3),
                    RegexHandler('^(ArteAndaluz)$', Espagnole4),
                
                    RegexHandler('^(Café du Soleil)$', Suisse1),
                    RegexHandler('^(Bayview)$', Suisse2),
                    RegexHandler('^(Coulouvrenière)$', Suisse3),
                    RegexHandler('^(Les Armures)$', Suisse4),
                    RegexHandler('^(Retour)$', restaurant_choix)
                ],

            CARTES:
                [
                    RegexHandler('^(Autres restaurants)$', restaurant_choix),
                    RegexHandler('^(GO)$', bienvenue)

                ],

                
           TRANSPORT:
                [ 
                    (MessageHandler(Filters.text, lieu_a_chercher)),
                    (MessageHandler(Filters.location, coordonnees_a_traiter)),
                    (CommandHandler('detail', afficher_departs)),

                ],
        },

        fallbacks=[CommandHandler('cancel', cancel)]

    )

    

    dp.add_handler(conv_handler_discussion)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if name == 'main':
    main()

